<?php
error_reporting(E_ALL);
// Считывание данных из файла.
include 'functions.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<td>Name</td>
					<td>Phone</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach (getProfiles() as $item) { ?>
					<tr>
						<td><?php echo $item['name']; ?></td>
						<td><?php echo $item['phoneNumber']; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</body>
</html>